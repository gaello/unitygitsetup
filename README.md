# Setting up Git and Git LFS for Unity Project

This repository contain example how you can set up git repository for a Unity project. Additionally, Git LFS was added.

You can read about it more in posts I've published at my blog: [Set git for a Unity project](https://www.patrykgalach.com/2020/12/28/git-repository-for-unity-project/) and [Enable Git LFS](https://www.patrykgalach.com/2021/01/04/enable-git-lfs-for-a-unity-project/)

Enjoy!

---

# How to use it?

Clone this repository to you computer or browse it online!

If you want, you can check [.gitignore](https://bitbucket.org/gaello/unitygitsetup/src/master/.gitattributes) and [.gitattributes](https://bitbucket.org/gaello/unitygitsetup/src/master/.gitignore) files

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
